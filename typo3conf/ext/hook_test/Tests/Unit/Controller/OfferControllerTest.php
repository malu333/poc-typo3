<?php
namespace Malu\HookTest\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Malu\HookTest\Controller\OfferController.
 *
 */
class OfferControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Malu\HookTest\Controller\OfferController
	 */
	protected $subject = NULL;

	public function setUp() {
		$this->subject = $this->getMock('Malu\\HookTest\\Controller\\OfferController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllOffersFromRepositoryAndAssignsThemToView() {

		$allOffers = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$offerRepository = $this->getMock('Malu\\HookTest\\Domain\\Repository\\OfferRepository', array('findAll'), array(), '', FALSE);
		$offerRepository->expects($this->once())->method('findAll')->will($this->returnValue($allOffers));
		$this->inject($this->subject, 'offerRepository', $offerRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('offers', $allOffers);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenOfferToView() {
		$offer = new \Malu\HookTest\Domain\Model\Offer();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('offer', $offer);

		$this->subject->showAction($offer);
	}

	/**
	 * @test
	 */
	public function newActionAssignsTheGivenOfferToView() {
		$offer = new \Malu\HookTest\Domain\Model\Offer();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('newOffer', $offer);
		$this->inject($this->subject, 'view', $view);

		$this->subject->newAction($offer);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenOfferToOfferRepository() {
		$offer = new \Malu\HookTest\Domain\Model\Offer();

		$offerRepository = $this->getMock('Malu\\HookTest\\Domain\\Repository\\OfferRepository', array('add'), array(), '', FALSE);
		$offerRepository->expects($this->once())->method('add')->with($offer);
		$this->inject($this->subject, 'offerRepository', $offerRepository);

		$this->subject->createAction($offer);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenOfferToView() {
		$offer = new \Malu\HookTest\Domain\Model\Offer();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('offer', $offer);

		$this->subject->editAction($offer);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenOfferInOfferRepository() {
		$offer = new \Malu\HookTest\Domain\Model\Offer();

		$offerRepository = $this->getMock('Malu\\HookTest\\Domain\\Repository\\OfferRepository', array('update'), array(), '', FALSE);
		$offerRepository->expects($this->once())->method('update')->with($offer);
		$this->inject($this->subject, 'offerRepository', $offerRepository);

		$this->subject->updateAction($offer);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenOfferFromOfferRepository() {
		$offer = new \Malu\HookTest\Domain\Model\Offer();

		$offerRepository = $this->getMock('Malu\\HookTest\\Domain\\Repository\\OfferRepository', array('remove'), array(), '', FALSE);
		$offerRepository->expects($this->once())->method('remove')->with($offer);
		$this->inject($this->subject, 'offerRepository', $offerRepository);

		$this->subject->deleteAction($offer);
	}
}
