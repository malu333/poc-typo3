<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Malu.' . $_EXTKEY,
	'Offer',
	array(
		'Offer' => 'list, show, new, create, edit, update, delete',
		
	),
	// non-cacheable actions
	array(
		'Offer' => 'create, update, delete',
		
	)
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = 'EXT:hook_test/Hooks/Hooks:saveRecord';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/tslib/class.tslib_menu.php']['filterMenuPages']['YourExtension\\Hook\\FilterMenuPages'] = 'EXT:your_extension/Classes/Hook/FilterMenuPages.php:YourExtension\Hook\FilterMenuPages';
